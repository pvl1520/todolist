import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';

import { Tasks } from '../api/tasks.js';

import './task.js';
import './body.html';

Template.body.onCreated(function bodyOnCreated() {
    this.state = new ReactiveDict();
    Meteor.subscribe('tasks');
});

Template.body.helpers({
    tasks: function () {
        const instance = Template.instance();
        if (instance.state.get('hideCompleted')) {
            // If hide completed is checked, filter tasks
            return Tasks.find({ checked: { $ne: true } }, { sort: { createdAt: -1 } });
        }
        // Otherwise, return all of the tasks
        return Tasks.find({}, {sort: {createdAt: -1 } });
    },
    incompleteCount() {
        return Tasks.find({ checked: { $ne: true } }).count();
    },
});

Template.body.events({
    'submit .new-task'(event) {
        // Prevent default browser form submit
        event.preventDefault();

        // Get value from form element
        const target = event.target;
        const text = target.text.value;

        // Insert a task into the collection
        Meteor.call('tasks.insert', text);

        // Clear form
        target.text.value = '';
    },
    'change .hide-completed input'(event, instance) {
        instance.state.set('hideCompleted', event.target.checked);
    },
    'click .modify'(event) {

        /*$(".new-task").submit(function(event) {
            // Prevent default browser form submit
            event.preventDefault();
        });*/

             $("input[name=text]").keydown(function(event){
                if(event.keyCode == 13) {
                    //event.preventDefault();
                    return false;
                }
            });


        // Get value from form element
        const txt1 = event.target.parentNode.lastElementChild.childNodes[2].data;
        const txt2 = ""; // Declare a variable for edited text
        const taskLiId = this._id; // Save _id of current task
        // Create new button for saving new task text
        const btnEd = $('<input type="button" id="saveEdit" class="edit" value="Save Editing"/>');
        $("form").append(btnEd);

        // Copy task text into form
        $("input[name=text]").val(txt1);

        $("#saveEdit").click(function() {
            txt2 = $("input[name=text]").val();
            console.log(txt1, txt2, taskLiId);

            // Modify a task
            // If entered string  is empty remain old task text
            if (txt2) {
                Meteor.call('tasks.modify', txt2, taskLiId);
            }
            // Else change task text for new one
            else {
                Meteor.call('tasks.modify', txt1, taskLiId);
            }

            // Clear form
            $("input[name=text]").val("");

            $("#saveEdit").remove();

            $("input[name=text]").off("keydown");
        });
    },

});

